# WxPy Calculator

## Table of Contents

- [About](#about)
- [Getting Started](#getting_started)
- [Prerequisites](#prerequisites)
- [Usage](#usage)
- [Contributing](./CONTRIBUTING.md)

## About

A calculator I made to learn more about GUI's and WxPython.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

This is python based, prerequisites are simple:
```shell
$ pip install requirements.txt
```

## Usage

After installing the [prerequisites](#prerequisites), just run:
```shell
$ cd src

$ python ./gui.py
```
or for a cli interface:
```shell
$ cd src

$ python ./cli.py
```
or if you don't want to use the command prompt, just double click on [gui.py](./src/gui.pyw) or [cli.py](./src/cli.py) in src.
