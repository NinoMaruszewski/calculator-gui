#!/usr/bin/env python3
import math


class Calculator(object):
    FUNCTIONS = ['add', 'subtract', 'multiply', 'divide', 'get previous answer']

    def __init__(self):
        self.previous_answer = 0
        self.history = []

    def add(self, a, b):
        result = a + b
        self.previous_answer = result
        self.history.append(result)
        return result

    def subtract(self, a, b):
        result = a - b
        self.previous_answer = result
        self.history.append(result)
        return result

    def multiply(self, a, b):
        result = a * b
        self.previous_answer = result
        self.history.append(result)
        return result

    def divide(self, a, b):
        result = a / b
        self.previous_answer = result
        self.history.append(result)
        return result

    def get_previous_ans(self):
        return self.previous_answer

    def load_history(self, hist: dict):
        self.history = hist['hist']
        self.previous_answer = hist["prev"]


if __name__ == '__main__':
    calc = Calculator()
    res = [calc.add(1, 1), calc.subtract(1, 2),
           calc.multiply(2, 5), calc.divide(5, 2)]
    print(*res, sep='\n')
    print('Last answer: {}'.format(calc.previous_answer))

    # print(dir(calc))
    # print([x for x in dir(calc) if not x.startswith("_")])
