#!/usr/bin/env python3
import pickle
import json
import wx
from wx.core import ICON_ERROR
from calculator import Calculator


class CalcFrame(wx.Frame):
    _STANDARD_SIZE = (500, 325)
    _SCIENTIFIC_SIZE = (500, 500)

    def __init__(self, parent, title, state, font_size=12):
        if state == 0:
            super(CalcFrame, self).__init__(parent, title=title,
                                            size=CalcFrame._STANDARD_SIZE)
        else:
            super(CalcFrame, self).__init__(parent, title=title,
                                            size=CalcFrame._SCIENTIFIC_SIZE)

        # Save variables
        self._state = state  # --> standard
        self._font_size = font_size
        self.a = 0
        self.b = 0
        self.op = lambda x: 0

        # font
        font = wx.Font(wx.FontInfo(self._font_size))
        self.SetFont(font)

        # icon
        self.SetIcon(wx.Icon("C:\\Users\\ninom\\Desktop\\Programing\\Python\\Calculator-gui\\calculator.ico"))

        # Calculator object
        self.calc = Calculator()

        # Menu bar
        menu_bar = wx.MenuBar()

        # file menu
        file_menu = wx.Menu()
        file_menu.Append(11, "&Save History", "Save history as a pickle")
        file_menu.Append(12, "Save History &As...",
                         "Choose a file format to save as")
        file_menu.AppendSeparator()
        file_menu.Append(14, "&Load History",
                         "Loads the history from a pickle")
        file_menu.AppendSeparator()
        file_menu.Append(16, "E&xit", "Quit the calculator")

        menu_bar.Append(file_menu, '&File')

        # view menu
        view_menu = wx.Menu()
        view_menu.Append(21, "&Standard", "Standard Calculator")
        view_menu.Append(22, "S&cientific", "Scientific Calculator")
        view_menu.AppendSeparator()
        view_menu.Append(24, "&History", "View results of past calculations")
        view_menu.AppendSeparator()
        view_menu.Append(25, "F&ont size", "Change font size")

        menu_bar.Append(view_menu, "&View")
        # help menu
        help_menu = wx.Menu()
        help_menu.Append(31, "&About...", "About the calculator")

        menu_bar.Append(help_menu, "&Help")

        self.SetMenuBar(menu_bar)

        # Menubar bindings:
        # file
        self.Bind(wx.EVT_MENU, self.on_save_history, id=11)
        self.Bind(wx.EVT_MENU, self.on_save_as_history, id=12)
        self.Bind(wx.EVT_MENU, self.on_load_history, id=14)
        self.Bind(wx.EVT_MENU, self.on_quit, id=16)

        # view
        self.Bind(wx.EVT_MENU, self.on_standard_mode, id=21)
        self.Bind(wx.EVT_MENU, self.on_scientific_mode, id=22)
        self.Bind(wx.EVT_MENU, self.on_change_font, id=25)

        # help
        self.Bind(wx.EVT_MENU, self.on_about, id=31)

        # Status bar
        self.CreateStatusBar()
        self.SetStatusText("Ready!")

        # Buttons
        panel = wx.Panel(self, -1)
        if self._state == 0:
            self.make_standard_buttons(panel)
        else:
            self.make_scientific_buttons(panel)

        # Show frame
        self.SetBackgroundColour('WHITE')
        self.Center()
        self.Show()

    def make_standard_buttons(self, panel):
        box_sizer = wx.BoxSizer(wx.VERTICAL)
        self.input_box = wx.TextCtrl(self, style=wx.TE_RIGHT | wx.TE_READONLY)
        box_sizer.Add(self.input_box, border=4,
                      flag=wx.EXPAND | wx.TOP | wx.BOTTOM)
        gs = wx.GridSizer(4, 5, 2)

        gs.AddMany([(wx.Button(self, id=81, label="Clear"), 0, wx.EXPAND),
                    (wx.StaticText(self), 0, wx.EXPAND),
                    (wx.StaticText(self), 0, wx.EXPAND),
                    (wx.Button(self, id=82, label="Ans"), 0, wx.EXPAND),
                    (wx.Button(self, id=41, label="7"), 0, wx.EXPAND),
                    (wx.Button(self, id=42, label="8"), 0, wx.EXPAND),
                    (wx.Button(self, id=43, label="9"), 0, wx.EXPAND),
                    (wx.Button(self, id=44, label="/"), 0, wx.EXPAND),
                    (wx.Button(self, id=51, label="4"), 0, wx.EXPAND),
                    (wx.Button(self, id=52, label="5"), 0, wx.EXPAND),
                    (wx.Button(self, id=53, label="6"), 0, wx.EXPAND),
                    (wx.Button(self, id=54, label="X"), 0, wx.EXPAND),
                    (wx.Button(self, id=61, label="1"), 0, wx.EXPAND),
                    (wx.Button(self, id=62, label="2"), 0, wx.EXPAND),
                    (wx.Button(self, id=63, label="3"), 0, wx.EXPAND),
                    (wx.Button(self, id=64, label="-"), 0, wx.EXPAND),
                    (wx.Button(self, id=71, label="0"), 0, wx.EXPAND),
                    (wx.Button(self, id=72, label="."), 0, wx.EXPAND),
                    (wx.Button(self, id=73, label="="), 0, wx.EXPAND),
                    (wx.Button(self, id=74, label="+"), 0, wx.EXPAND)])

        box_sizer.Add(gs, proportion=1, flag=wx.EXPAND)
        self.SetSizer(box_sizer)

        self.Bind(wx.EVT_BUTTON,
                  lambda e: self.input_box.WriteText('0'), id=71)
        self.Bind(wx.EVT_BUTTON,
                  lambda e: self.input_box.WriteText('1'), id=61)
        self.Bind(wx.EVT_BUTTON,
                  lambda e: self.input_box.WriteText('2'), id=62)
        self.Bind(wx.EVT_BUTTON,
                  lambda e: self.input_box.WriteText('3'), id=63)
        self.Bind(wx.EVT_BUTTON,
                  lambda e: self.input_box.WriteText('4'), id=51)
        self.Bind(wx.EVT_BUTTON,
                  lambda e: self.input_box.WriteText('5'), id=52)
        self.Bind(wx.EVT_BUTTON,
                  lambda e: self.input_box.WriteText('6'), id=53)
        self.Bind(wx.EVT_BUTTON,
                  lambda e: self.input_box.WriteText('7'), id=41)
        self.Bind(wx.EVT_BUTTON,
                  lambda e: self.input_box.WriteText('8'), id=42)
        self.Bind(wx.EVT_BUTTON,
                  lambda e: self.input_box.WriteText('9'), id=43)
        self.Bind(wx.EVT_BUTTON,
                  lambda e: self.input_box.WriteText('.'), id=72)

        self.Bind(wx.EVT_BUTTON, self.on_add_press, id=74)
        self.Bind(wx.EVT_BUTTON, self.on_sub_press, id=64)
        self.Bind(wx.EVT_BUTTON, self.on_mult_press, id=54)
        self.Bind(wx.EVT_BUTTON, self.on_div_press, id=44)
        self.Bind(wx.EVT_BUTTON, self.on_eq_press, id=73)

        self.Bind(wx.EVT_BUTTON, lambda e: self.input_box.Clear(), id=81)
        self.Bind(wx.EVT_BUTTON,
                  lambda e: self.input_box.SetValue(
                      str(self.calc.previous_answer)),
                  id=82)

    def make_scientific_buttons(self, panel):
        button = wx.Button(panel, -1, "Button1")

    def on_save_history(self, event):
        with wx.FileDialog(self,
                           "Save History file",
                           defaultFile="history.pickle",
                           wildcard="Pickle Files (*.pickle)|*.pickle",
                           style=wx.FD_SAVE | wx.FD_OVERWRITE_PROMPT) as file_dialog:

            if file_dialog.ShowModal() == wx.ID_CANCEL:
                return  # user changed their mind

            # save the current contents in the file
            pathname = file_dialog.GetPath()
            try:
                with open(pathname, 'wb') as file:
                    to_save = {"hist": self.calc.history,
                               "prev": self.calc.previous_answer}
                    pickle.dump(to_save, file)
            except IOError:
                wx.LogError("Cannot save current data in file {}"
                            .format(pathname))

    def on_save_as_history(self, event):
        with wx.SingleChoiceDialog(self,
                                   "Choose a file format:",
                                   'History save format',
                                   ['pickle', 'json', 'text']) as choice_dlg:

            if choice_dlg.ShowModal() == wx.ID_CANCEL:
                return
            else:
                response = choice_dlg.GetStringSelection()

            if response == 'pickle':
                self.on_save_history(None)
            elif response == 'json':
                self._save_json()
            elif response == 'text':
                self._save_txt()

    def on_load_history(self, event):
        with wx.FileDialog(self,
                           "Load History file",
                           wildcard="Pickle Files (*.pickle)|*.pickle",
                           style=wx.FD_OPEN | wx.FD_FILE_MUST_EXIST) as file_dlg:

            if file_dlg.ShowModal() == wx.ID_CANCEL:
                return

            pathname = file_dlg.GetPath()
            try:
                with open(pathname, 'rb') as file:
                    loaded_hist = pickle.load(file)
                    self.calc.load_history(loaded_hist)
            except IOError:
                wx.LogError("Cannot open file {}.".format(pathname))

    def on_quit(self, event):
        self.Close()

    def on_standard_mode(self, event):
        if self._state == 0:
            return
        else:
            self._switch()

    def on_scientific_mode(self, event):
        if self._state == 1:
            return
        else:
            self._switch()

    def on_about(self, event):
        wx.MessageBox("This is a calculator written in "
                      "Python and Gui'd with WxPython.",
                      "About WxPy Calculator",
                      wx.OK | wx.ICON_INFORMATION,
                      self)

    def _save_json(self):
        with wx.FileDialog(self,
                           "Save History file",
                           defaultFile="history.json",
                           wildcard="JSON Files (*.json)|*.json",
                           style=wx.FD_SAVE | wx.FD_OVERWRITE_PROMPT) as file_dialog:

            if file_dialog.ShowModal() == wx.ID_CANCEL:
                return  # user changed their mind

            # save the current contents in the file
            pathname = file_dialog.GetPath()
            try:
                with open(pathname, 'w') as file:
                    to_save = {"hist": self.calc.history,
                               "prev": self.calc.previous_answer}
                    json.dump(to_save, file, indent=4)
            except IOError:
                wx.LogError("Cannot save current data in file {}"
                            .format(pathname))

    def _save_txt(self):
        with wx.FileDialog(self,
                           "Save History file",
                           defaultFile="history.txt",
                           wildcard="Text Files (*.txt)|*.txt",
                           style=wx.FD_SAVE | wx.FD_OVERWRITE_PROMPT) as file_dialog:

            if file_dialog.ShowModal() == wx.ID_CANCEL:
                return  # user changed their mind

            # save the current contents in the file
            pathname = file_dialog.GetPath()
            try:
                with open(pathname, 'w') as file:
                    to_save = {"hist": self.calc.history,
                               "prev": self.calc.previous_answer}
                    file.write(json.dumps(to_save, indent=4))
                    file.close()
            except IOError:
                wx.LogError("Cannot save current data in file {}"
                            .format(pathname))

    def _switch(self):
        if self._state == 0:
            CalcFrame(None, "WxPy Scientific Calculator", 1, self._font_size)
        else:
            CalcFrame(None, "WxPy Calculator", 0, self._font_size)
        self.Destroy()

    def on_add_press(self, event):
        self.a = self.input_box.GetValue()
        self.input_box.Clear()
        self.op = self.calc.add

    def on_sub_press(self, event):
        self.a = self.input_box.GetValue()
        self.input_box.Clear()
        self.op = self.calc.subtract

    def on_mult_press(self, event):
        self.a = self.input_box.GetValue()
        self.input_box.Clear()
        self.op = self.calc.multiply

    def on_div_press(self, event):
        self.a = self.input_box.GetValue()
        self.input_box.Clear()
        self.op = self.calc.divide

    def on_eq_press(self, event):
        self.b = self.input_box.GetValue()
        result = self.op(float(self.a), float(self.b))
        self.input_box.SetValue(str(result))

    def on_change_font(self, event):
        with wx.TextEntryDialog(self,
                                "Enter a font size, current is {}".format(
                                    self._font_size),
                                "Font size changer",
                                str(self._font_size)) as font_dlg:

            if font_dlg.ShowModal() == wx.ID_CANCEL:
                return
            elif font_dlg.ShowModal() == wx.ID_OK:
                try:
                    font_new = int(font_dlg.GetValue())
                except ValueError:
                    wx.MessageBox('Please enter a valid font size',
                                  'Input Error',
                                  wx.OK | wx.ICON_ERROR,
                                  self)

                if (font_new >= 0 and font_new <= 70):
                    if self._state == 0:
                        CalcFrame(None, "WxPy Calculator",
                                  self._state, font_size=font_new)
                        self.Destroy()
                    else:
                        CalcFrame(None, "WxPy Scientific Calculator",
                                  self._state, font_size=font_new)
                        self.Destroy()

                else:
                    wx.MessageBox('Please enter a valid font size',
                                  'Input Error',
                                  wx.OK | wx.ICON_ERROR,
                                  self)


if __name__ == '__main__':
    app = wx.App()
    CalcFrame(None, "WxPy Calculator", 0)
    app.MainLoop()
