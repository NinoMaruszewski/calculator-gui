#!/usr/bin/env python3

from calculator import Calculator

calc = Calculator()

while True:
    # Ask what to do
    for i in range(len(Calculator.FUNCTIONS)):
        print("{}. {}".format(i, Calculator.FUNCTIONS[i]))

    try:
        selection = int(input("Select an option: "))
    except ValueError:
        print('Please enter a valid value')
        continue
    finally:
        pass

    if selection == 0:
        a = int(input("First number to add: "))
        b = int(input("Second number to add: "))
        print(calc.add(a, b))
    elif selection == 1:
        a = int(input("First number to subtract: "))
        b = int(input("Second number to subtract: "))
        print(calc.subtract(a, b))
    elif selection == 2:
        a = int(input("First number to multiply: "))
        b = int(input("Second number to multiply: "))
        print(calc.multiply(a, b))
    elif selection == 3:
        a = int(input("First number to divide: "))
        b = int(input("Second number to divide: "))
        print(calc.divide(a, b))
    else:
        print(calc.get_previous_ans())
